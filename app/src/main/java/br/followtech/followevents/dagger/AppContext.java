package br.followtech.followevents.dagger;

/**
 * Created by diegocunha on 15/12/16.
 */

public class AppContext {

    private static AppComponent component;


    public static AppComponent getComponent() {
        return component;
    }

    public static void setComponent(AppComponent component) {
        AppContext.component = component;
    }
}
