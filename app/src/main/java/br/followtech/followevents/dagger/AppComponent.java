package br.followtech.followevents.dagger;

import javax.inject.Singleton;

import br.followtech.followevents.FollowEvents;
import dagger.Component;

/**
 * Created by diegocunha on 15/12/16.
 */

@Singleton
@Component(modules = {AppModule.class, ServiceModule.class})
public interface AppComponent {
    void inject(FollowEvents app);
}
