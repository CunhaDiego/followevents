package br.followtech.followevents.helper;

/**
 * Created by diegocunha on 15/12/16.
 */

public interface ResponseCallBack<T> {
    void onComplete(T response, ServiceError error);
}
