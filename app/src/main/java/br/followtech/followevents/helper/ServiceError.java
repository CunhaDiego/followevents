package br.followtech.followevents.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by diegocunha on 15/12/16.
 */

public class ServiceError {

    public static final int SERVER_ERROR = -100;
    public static final int UNKNOW_ERROR = 0;

    @SerializedName("errorCode")
    private int code;
    @SerializedName("errorMsg")
    private String message;
    private boolean network;

    public ServiceError() { }

    public ServiceError(int code, String message, boolean network) {
        this.code = code;
        this.message = message;
        this.network = network;
    }

    public ServiceError(int code) {
        this(code, null, false);
    }

    public ServiceError(int code, boolean network) {
        this(code, null, network);
    }

    public ServiceError(int code, String message) {
        this(code, message, false);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isNetwork() {
        return network;
    }

    public void setNetwork(boolean network) {
        this.network = network;
    }
}
