package br.followtech.followevents.helper;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;

import java.util.Date;

/**
 * Created by diegocunha on 15/12/16.
 */

public class DateHelper {

    public static Date getToday() {
        return DateTime.now().toDate();
    }

    public static boolean lessEqualThan(Date a, Date b) {
        return compare(a, b) <= 0;
    }

    public static boolean lessThan(Date a, Date b) {
        return compare(a, b) < 0;
    }

    public static boolean greaterThan(Date a, Date b) {
        return compare(a, b) > 0;
    }

    public static boolean greaterEqualThan(Date a, Date b) {
        return compare(a, b) >= 0;
    }

    private static int compare(Date a, Date b) {
        return DateTimeComparator.getDateOnlyInstance().compare(new DateTime(a), new DateTime(b));
    }
}
