package br.followtech.followevents.helper;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.lang.annotation.Annotation;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by diegocunha on 15/12/16.
 */

public class ServiceErrorConverter {

    private Converter<ResponseBody, ServiceError> converter;

    private Application app;

    @Inject
    public ServiceErrorConverter(Application app, Retrofit retrofit) {
        this.app = app;
        converter = retrofit.responseBodyConverter(ServiceError.class, new Annotation[0]);
    }

    public ServiceError parseError(Response<?> response) {
        ServiceError error;

        try {
            error = converter.convert(response.errorBody());
            error.setCode(response.code());
            error.setNetwork(!isOnline());
        } catch (Exception e) {
            error = response != null ? new ServiceError(response.code(), !isOnline()) : null;
            e.printStackTrace();
        }

        return error;
    }

    public ServiceError parseError(Throwable t) {
        ServiceError error = new ServiceError();
        error.setCode(-1);
        error.setMessage(t.getMessage());
        error.setNetwork(!isOnline());

        return error;
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) app.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
