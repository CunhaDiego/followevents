package br.followtech.followevents;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

import br.followtech.followevents.dagger.AppComponent;
import br.followtech.followevents.dagger.AppContext;
import br.followtech.followevents.dagger.AppModule;
import br.followtech.followevents.dagger.DaggerAppComponent;
import br.followtech.followevents.dagger.ServiceModule;

/**
 * Created by diegocunha on 14/12/16.
 */

public class FollowEvents extends Application {

    @Override
    public void onCreate() {
        super.onCreate();


        AppComponent component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .serviceModule(new ServiceModule())
                .build();

        AppContext.setComponent(component);
        AppContext.getComponent().inject(this);
        JodaTimeAndroid.init(this);
    }
}
