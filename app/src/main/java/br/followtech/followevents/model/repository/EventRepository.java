package br.followtech.followevents.model.repository;

import java.util.List;

import javax.inject.Inject;

import br.followtech.followevents.helper.ResponseCallBack;
import br.followtech.followevents.model.object.Event;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by diegocunha on 15/12/16.
 */

public class EventRepository {

    private Realm realm;

    @Inject
    public EventRepository(Realm realm) {
        this.realm = realm;
    }

    private boolean hasEvents() {
        return realm.where(Event.class).findAll().size() > 0;
    }


    public RealmResults<Event> findAllEvents() {
        if (hasEvents()) {
            return realm.where(Event.class).findAll();
        }

        return null;
    }

    public void getEvents(final ResponseCallBack<List<Event>> callBack) {
        callBack.onComplete(findAllEvents(), null);
    }


    public void getEventById(String id, final ResponseCallBack<Event> callBack){
        if(hasEvents()){
            Event event = realm.where(Event.class).equalTo("id", id).findFirst();

            callBack.onComplete(event, null);
        }
    }
}
