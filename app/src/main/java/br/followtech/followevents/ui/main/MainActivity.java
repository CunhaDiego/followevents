package br.followtech.followevents.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import br.followtech.followevents.R;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.design.widget.BottomNavigationView.OnNavigationItemSelectedListener;

public class MainActivity extends AppCompatActivity implements MainView, OnNavigationItemSelectedListener {

    @BindView(R.id.bottom_navigation)
    protected BottomNavigationView menuView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        menuView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_main:
                break;
            case R.id.action_events:
                break;
            case R.id.action_nearby:
                break;
            case R.id.action_profile:
                break;
        }

        return false;
    }
}
